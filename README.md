# Panda

- https://gist.github.com/lambda2/d68d20756ea9bafcf9c60b28b9161049
- https://api.pandascore.co/rest
- https://api.pandascore.co/doc#v2teams
- https://devdocs.io

```shell
kerl build 20.3.6 20.3.6
kerl install 20.3.6 ~/.kerl/otp/20.3.6

kiex install v1.6
kiex default v1.6

mix deps.get
iex -S mix
```

```elexir
0 pandascore.git master (20.3.6) ∀ make debug
mix format
mix deps.get
Resolving Hex dependencies...
Dependency resolution completed:
Unchanged:
  parse_trans 3.2.0
  poison 3.1.0
  unicode_util_compat 0.3.1
All dependencies up to date
iex -S mix
Erlang/OTP 20 [erts-9.3.1] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:10] [hipe] [kernel-poll:false]


18:50:48.730 [debug] fetching https://api.pandascore.co/matches/past?page=1&per_page=100
Interactive Elixir (1.6.5) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> Panda.odds_for_match 49348
18:50:49.421 [debug] done fetching https://api.pandascore.co/matches/past?page=1&per_page=100
 
18:50:49.664 [info]  one page fetched, 123 to go!
 
18:50:49.667 [debug] fetching https://api.pandascore.co/matches/past?page=2&per_page=100
 
18:50:49.667 [debug] fetching https://api.pandascore.co/matches/past?page=3&per_page=100
 
18:50:49.918 [debug] done fetching https://api.pandascore.co/matches/past?page=2&per_page=100
 
18:50:49.992 [debug] done fetching https://api.pandascore.co/matches/past?page=3&per_page=100
 
18:50:50.152 [info]  fetched 2 pages!
 
18:50:50.152 [debug] last page number: 123

18:50:50.152 [debug] adding 204 entries to the cache

18:50:50.152 [debug] adding 206 entries to the cache

18:50:50.152 [debug] adding 205 entries to the cache
iex(1)> Panda.upcoming_matches                                                                                                                                                                                                                                                                                                                                                                              18:51:06.167 [debug] fetching https://api.pandascore.co/matches/upcoming?per_page=5                                                                                                                                                                                                                                                                                                                         18:51:06.439 [debug] done fetching https://api.pandascore.co/matches/upcoming?per_page=5                                                                                                              [                                                                                                                                                                                                       %{                                                                                                                                                                                                      "begin_at" => "2018-06-01T23:00:00Z",                                                                                                                                                                 "id" => 20589,                                                                                                                                                                                        "name" => "Houston Outlaws vs Shanghai Dragons"                                                                                                                                                     },                                                                                                                                                                                                    %{                                                                  
    "begin_at" => "2018-06-02T01:00:00Z",
    "id" => 20590,
    "name" => "Florida Mayhem vs Seoul Dynasty"
  },
  %{
    "begin_at" => "2018-06-02T02:00:00Z",
    "id" => 49359,
    "name" => "Upper Semifinal"
  },
  %{
    "begin_at" => "2018-06-02T02:00:00Z",
    "id" => 49358,
    "name" => "Upper Semifinal"
  },
  %{
    "begin_at" => "2018-06-02T02:00:00Z",
    "id" => 49354,
    "name" => "Upper Semifinal"
  }
]
iex(2)> Panda.odds_for_match 49354 

18:51:20.587 [debug] fetching https://api.pandascore.co/matches/49354/opponents
 
18:51:20.798 [debug] done fetching https://api.pandascore.co/matches/49354/opponents
 
18:51:20.798 [debug] fetching https://api.pandascore.co/teams/1652
 
18:51:20.880 [debug] done fetching https://api.pandascore.co/teams/1652
 
18:51:20.881 [debug] fetching https://api.pandascore.co/teams/1699
 
18:51:20.958 [debug] done fetching https://api.pandascore.co/teams/1699
%{"Natus Vincere" => 0.5230275953112038, "Newbee" => 0.4769724046887961}
```
