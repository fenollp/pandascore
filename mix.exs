defmodule Panda.MixProject do
  use Mix.Project

  def project do
    [
      app: :pandascore,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Panda.Application, []},
      extra_applications: [
        :logger,
        :hackney
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:hackney, "~> 1.12"},
      # https://github.com/benoitc/hackney/pull/456
      {:hackney, git: "https://github.com/jimdigriz/hackney.git", branch: "zlib"},
      {:poison, "~> 3.1"}
    ]
  end
end
