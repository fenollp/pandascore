defmodule Panda do
  @moduledoc """
  Documentation for Panda.
  """

  @spec upcoming_matches() :: [map()] | {:error, any()}
  def upcoming_matches do
    {:ok, matches} = Panda.API.get_extract("/matches/upcoming?per_page=5")
    keys = ["id", "name", "begin_at"]

    for match <- matches do
      Map.take(match, keys)
    end
  end

  def odds_for_match(match) do
    {:ok, %{"opponents" => opponents}} = Panda.API.get_extract("/matches/#{match}/opponents")
    [a, b] = Enum.map(opponents, fn opponent -> opponent["id"] end)
    # [a,b] = Enum.map(opponents, &(&1["id"))
    # [a,b] = for opponent <- opponents, do: opponent["id"]
    [aName, bName] = Enum.map([a, b], &get_team_name/1)

    with {:ok, odds} <- Panda.RPI.odds(a, b),
         do: %{aName => odds[a], bName => odds[b]}
  end

  defp get_team_name(team) do
    {:ok, %{"name" => name}} = Panda.API.get_extract("/teams/#{team}")
    name
  end

  def reset_cache do
    {sup, id} = {Panda.Supervisor, Panda.RPI.Cache}

    with :ok <- Supervisor.terminate_child(sup, id),
         do: Supervisor.restart_child(Panda.Supervisor, id)
  end
end
