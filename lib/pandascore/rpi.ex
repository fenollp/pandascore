defmodule Panda.RPI do
  # https://en.wikipedia.org/wiki/Rating_percentage_index
  # RPI = (WP * 0.25) + (OWP * 0.50) + (OOWP * 0.25)
  # where WP is Winning Percentage, OWP is Opponents' Winning Percentage
  #   and OOWP is Opponents' Opponents' Winning Percentage.
  # The WP is calculated by taking a team's wins divided by the number
  #   of games it has played (i.e. wins plus losses).

  def odds(a, b), do: GenServer.call(Panda.RPI.Cache, {:odds, a, :v, b})
end

defmodule Panda.RPI.Cache do
  use GenServer
  require Logger

  @type team() :: pos_integer()
  @type t() :: %{
          won: non_neg_integer(),
          # draw: non_neg_integer(), could not find an example so leaving it out
          lost: non_neg_integer(),
          oppo: MapSet.t(team())
        }
  @type state() :: %{pristine: boolean()}

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def update_cache do
    GenServer.cast(__MODULE__, :update_cache)
  end

  @impl true
  def init(_) do
    update_cache()
    emptyState = %{pristine: true}
    {:ok, emptyState}
  end

  @spec rpi(team()) :: float()
  defp rpi(a) do
    # WP
    %{won: aWon, lost: aLost, oppo: aOppo} = Process.get(a)
    aWP = aWon / (aWon + aLost)

    # OWP
    folder = fn team, {wps, oppoOppo} ->
      %{won: won, lost: lost, oppo: oppo} = Process.get(team)
      wp = won / (won + lost)
      {[wp | wps], MapSet.union(oppoOppo, oppo)}
    end

    acc0 = {[], MapSet.new([])}
    {oWPs, oppoOppo} = Enum.reduce(aOppo, acc0, folder)
    oWP = Enum.sum(oWPs) / length(oWPs)

    # OOWP
    mapper = fn team ->
      %{won: won, lost: lost} = Process.get(team)
      won / (won + lost)
    end

    ooWPs = Enum.map(oppoOppo, mapper)
    ooWP = Enum.sum(ooWPs) / length(ooWPs)

    # RPI
    0.25 * aWP + 0.50 * oWP + 0.25 * ooWP
  end

  @impl true
  def handle_call({:odds, _, :v, _}, _from, state = %{pristine: true}) do
    {:reply, {:error, :empty_cache}, state}
  end

  def handle_call({:odds, a, :v, b}, _from, state) do
    {rpiA, rpiB} = {rpi(a), rpi(b)}
    base = 1 / (rpiA + rpiB)
    odds = %{a => base * rpiA, b => base * rpiB}
    {:reply, {:ok, odds}, state}
  end

  @impl true
  def handle_cast(:update_cache, state) do
    {:ok, last} = cache_all_past_matches()
    Logger.debug("last page number: #{last}")
    newState = Map.put(state, :pristine, false)
    {:noreply, newState}
  end

  def handle_cast({:store, kvs}, state) do
    Logger.debug("adding #{length(kvs)} entries to the cache")
    :lists.foreach(&store_history/1, kvs)
    {:noreply, state}
  end

  defp store_history({team, newHistory}) do
    data =
      case Process.get(team) do
        nil -> newHistory
        history -> merge_histories(newHistory, history)
      end

    Process.put(team, data)
  end

  defp merge_histories(%{won: wA, lost: lA, oppo: oA}, %{won: wB, lost: lB, oppo: oB}) do
    %{won: wA + wB, lost: lA + lB, oppo: MapSet.union(oA, oB)}
  end

  defp cache_all_past_matches do
    {:ok, last} = store_past_matches(1, true)

    Logger.info("one page fetched, #{last} to go!")
    # NOTE: the cache being pretty hungry I artificially limit the amount of pages to crawl
    # pages = Enum.to_list(2..last)
    pages =
      [2, 3]
      |> Enum.map(fn page -> Task.async(fn -> store_past_matches(page) end) end)
      |> Enum.map(&Task.await/1)

    Logger.info("fetched #{length(pages)} pages!")
    {:ok, last}
  end

  defp store_past_matches(page, get_last \\ false) do
    url = "/matches/past?page=#{page}&per_page=100"
    {:ok, 200, headers, ref} = Panda.API.get(url)
    {:ok, data} = :hackney.body(ref)
    {:ok, matches} = Poison.decode(data)
    results = Enum.flat_map(matches, &unmarshal/1)
    :ok = store(results)

    case get_last do
      false -> :ok
      true -> {:ok, String.to_integer(last(headers))}
    end
  end

  defp store(past_matches) when is_list(past_matches) do
    GenServer.cast(__MODULE__, {:store, past_matches})
  end

  defp unmarshal(match) do
    winner = match["winner"]["id"]

    opponents =
      match["opponents"]
      |> Enum.map(fn oppo -> oppo["id"] end)
      |> MapSet.new()

    [
      {winner, %{won: 1, lost: 0, oppo: MapSet.delete(opponents, winner)}}
      | for team <- MapSet.delete(opponents, winner) do
          history = %{won: 0, lost: 1, oppo: MapSet.delete(opponents, team)}
          {team, history}
        end
    ]
  end

  @regexp_last Regex.compile("page=([0-9]+)[^>]*>; rel=\"last\"")
  defp last(headers) do
    # Map.new(headers)["Link"]
    link = :proplists.get_value("Link", headers)
    {:ok, re} = @regexp_last
    [page] = Regex.run(re, link, capture: :all_but_first)
    page
  end
end
