defmodule Panda.Application do
  @moduledoc false
  use Application

  def start(_type, _args) do
    Panda.Supervisor.start_link(name: Panda.Supervisor)
  end
end

defmodule Panda.Supervisor do
  @moduledoc false
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      Panda.RPI.Cache
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
