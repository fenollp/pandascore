defmodule Panda.API do
  require Logger
  @token "M7rjkX9pE0qT2OJCCWFZ-c2fH0pJWBgphUH68X6isSJYRxNvN0w"
  # todo: retry up to @retries times when returns {:error,:timeout}
  @retries 5
  # todo: use as timeout multiplier when retrying
  @backoff 2

  # todo: add an optional boolean parameter called`cacheable?`
  # that defaults to false and activate it on some get | get_extract calls.
  def get(query) do
    url = "https://api.pandascore.co" <> query
    headers = [{"Authorization", "Bearer #{@token}"}]

    options = [
      {:pool, :default},
      :compress,
      # NOTE: please use Let's Encrypt instead of self-signed certs,
      # some clients would probably just disable TLS entirely!
      {:ssl_options, [{:versions, [:"tlsv1.2"]}]}
    ]

    Logger.debug("fetching #{url}")
    r = :hackney.get(url, headers, "", options)
    Logger.debug("done fetching #{url}")
    r
  end

  def get_extract(query) do
    with {:ok, 200, _, ref} <- get(query),
         {:ok, data} <- :hackney.body(ref),
         do: Poison.decode(data)
  end
end
