all: lint
	mix compile

lint:
	mix format

debug: lint
	mix deps.get
	iex -S mix
